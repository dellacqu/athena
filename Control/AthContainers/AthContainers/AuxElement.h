// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
/**
 * @file AthContainers/AuxElement.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Jan, 2013
 * @brief Base class for elements of a container that can have aux data.
 */


#ifndef ATHCONTAINERS_AUXELEMENTBASE_H
#define ATHCONTAINERS_AUXELEMENTBASE_H


#include "AthContainersInterfaces/IAuxElement.h"
#include "AthContainersInterfaces/IConstAuxStore.h"
#include "AthContainersInterfaces/IAuxStore.h"
#include "AthLinks/DataLink.h"
#include "AthContainers/AuxTypeRegistry.h"
#include "AthContainers/AuxVectorData.h"
#include "AthContainers/tools/AuxDataTraits.h"
#include "AthContainers/TypelessConstAccessor.h"
#include "AthContainers/ConstAccessor.h"
#include "AthContainers/Accessor.h"
#include "AthContainers/Decorator.h"
#include "AthContainers/exceptions.h"
#include "AthContainers/tools/likely.h"
#include "CxxUtils/span.h"
#include <cstddef>


namespace SG {


class AuxElement;
class AuxElementData;
class AuxElementStandaloneData;
class AuxVectorData_test;
class AuxVectorBase;
class AuxVectorBase_test;


/**
 * @brief Const part of @c AuxElement.
 *
 * This base class factors out the const portions of @c AuxElement
 * (which see for a complete description).
 */
class ConstAuxElement
  : public SG::IAuxElement
{
public:
  /// Default constructor.
  ConstAuxElement();


  /**
   * @brief Constructor with explicit container / index.
   * @param container Container of which this element will be a part.
   * @param index Index of this element within the container.
   *
   * This does not make any changes to aux data.
   */
  ConstAuxElement (const SG::AuxVectorData* container,  size_t index);


  /**
   * @brief Copy Constructor.
   * @param other Object being copied.
   *
   * We do not copy the container/index --- the new object is not yet
   * in a container!
   *
   * In the case of constructing an object with a private store,
   * @c makePrivateStore will take care of copying the aux data.
   */
  ConstAuxElement (const ConstAuxElement& other);


  /// No assignment to a const element.
  ConstAuxElement& operator= (const ConstAuxElement& other) = delete;


  /**
   * @brief Destructor.
   *
   * Any private store is deleted.
   */
  ~ConstAuxElement();


  /**
   * @brief Return the container holding this element.
   */
  const SG::AuxVectorData* container() const;


  /**
   * @brief Return the index of this element within its container.
   *
   * Inherited from IAuxElement.
   */
  using IAuxElement::index;


  /// Helper class to provide const generic access to aux data.
  using TypelessConstAccessor = SG::TypelessConstAccessor;

  /// Helper class to provide type-safe access to aux data.
  template <class T, class ALLOC = AuxAllocator_t<T> >
  using ConstAccessor = SG::ConstAccessor<T, ALLOC>;

  /// class to provide type-safe access to aux data.
  template <class T, class ALLOC = AuxAllocator_t<T> >
  using Decorator = SG::Decorator<T, ALLOC>;


  /**
   * @brief Fetch an aux data variable, as a const reference.
   * @param name Name of the aux variable.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor
   * or @c ConstAccessor classes.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename ConstAccessor<T, ALLOC>::const_reference_type
  auxdata (const std::string& name) const;


  /**
   * @brief Fetch an aux data variable, as a const reference.
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor
   * or @c ConstAccessor classes.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename ConstAccessor<T, ALLOC>::const_reference_type
  auxdata (const std::string& name,
           const std::string& clsname) const;


  /**
   * @brief Fetch an aux data variable, as a const reference.
   * @param name Name of the aux variable.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c ConstAccessor class.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename ConstAccessor<T, ALLOC>::const_reference_type
  auxdataConst (const std::string& name) const;


  /**
   * @brief Fetch an aux data variable, as a const reference.
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c ConstAccessor class.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename ConstAccessor<T, ALLOC>::const_reference_type
  auxdataConst (const std::string& name,
                const std::string& clsname) const;


  /**
   * @brief Check if an aux variable is available for reading
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor class.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  bool isAvailable (const std::string& name,
                    const std::string& clsname = "") const;


  /**
   * @brief Check if an aux variable is available for writing as a decoration.
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor class.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  bool isAvailableWritableAsDecoration (const std::string& name,
                                        const std::string& clsname = "") const;


  /**
   * @brief Fetch an aux decoration, as a non-const reference.
   * @param name Name of the aux variable.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor class.
   *
   * If the container is locked, this will allow fetching only variables
   * that do not yet exist (in which case they will be marked as decorations)
   * or variables already marked as decorations.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename Decorator<T, ALLOC>::reference_type
  auxdecor (const std::string& name) const;


  /**
   * @brief Fetch an aux decoration, as a non-const reference.
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor class.
   *
   * If the container is locked, this will allow fetching only variables
   * that do not yet exist (in which case they will be marked as decorations)
   * or variables already marked as decorations.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename Decorator<T, ALLOC>::reference_type
  auxdecor (const std::string& name,
            const std::string& clsname) const;


  /**
   * @brief Return a set of identifiers for existing data items
   *        for this object.
   *
   *        If this object has a private or standalone store, then information
   *        from that will be returned.  Otherwise, if this element
   *        is part of a container, then information for the container
   *        will be returned.  Otherwise, return an empty set.
   */
  const SG::auxid_set_t& getAuxIDs() const;


private:
  friend class AuxElement;
  friend class SG::AuxVectorBase;


  /**
   * @brief Out-of-line portion of destructor.
   *
   * Delete a private store if we have one.
   */
  void releasePrivateStoreForDtor();


  /**
   * @brief Set the index/container for this element.
   * @param index The index of this object within the container.
   * @param container The container holding this object.
   *                  May be null if this object is being removed
   *                  from a container.
   *
   * Usually this simply sets the index and container members
   * of this object.  However, in the case where this object has
   * an associated private store, then we need to deal with
   * releasing the store if the object is being added to a container,
   * or making a new store if the object is being removed from a container.
   */
  void setIndex (size_t index, const SG::AuxVectorData* container);


  /**
   * @brief Set the index/container for this element.
   * @param index The index of this object within the container.
   * @param container The container holding this object.
   *                  May be null if this object is being removed
   *                  from a container.
   *
   * This is called from @c setIndex when we have a private store to deal with.
   */
  bool setIndexPrivate (size_t index, const SG::AuxVectorData* container);


  /// The container of which this object is an element.
  /// Should be null if this object is not within a container,
  /// except that it may also point at a private store.
  const SG::AuxVectorData* m_container;
};


//***************************************************************************


/**
 * @brief Base class for elements of a container that can have aux data.
 *
 * Classes that want to have associated auxiliary data should derive
 * from this class.   (It is also possible to use this class directly,
 * if you want a container that _only_ stores auxiliary data.)
 *
 * The first thing that happens when you derive from @c AuxElement
 * is that when an object is inserted into a @c DataVector, the
 * vector will maintain information in the object telling were
 * it is within the vector.  For example:
 *
 *@code
 *  DataVector<SG::AuxElement> v (2);
 *  v[1] = new SG::AuxElement;
 *  assert (v[1]->index() == 1);
 *  assert (v[1]=>container() == &v);
 @endcode
 *
 * As long as you don't use @c DataVector::stdcont or use unsafe
 * casts, @c DataVector will correctly maintain this information.
 *
 * When an object deriving from @c AuxElement is in a @c DataVector
 * it may have auxiliary data associated with it; that is,
 * named data objects of arbitrary type.  The recommended way
 * of accessing auxiliary data is through the @c Accessor
 * and @c ConstAccessor classes, which cache the lookup between
 * the aux data item name and its internal representation.
 * The difference between these two is that @c ConstAccessor allows
 * only const access to the element, while @c Accessor, which derives
 * from it, allows non-const access as well.  A given name must always
 * have the same type, no matter where it is used (even across
 * different classes); otherwise, an exception will be thrown.
 * To help prevent conflicts between different classes, aux data item
 * names may be optionally qualified with a class name.
 * Here's an example of using @c ConstAccessor:
 *
 *@code
 *   // Only need to do this once.
 *   SG::ConstAccessor<int> vint1 ("myInt");
 *   ...
 *   const Myclass* m = ...;
 *   int x = vint1 (*m);
 @endcode
 *
 * The @c auxdata methods can be used as a shortcut for this,
 * but that's not recommended for anything for which performance
 * is an issue.
 *
 * You can also define getters/setters in your class:
 *
 *@code
 *  class Myclass {
 *    ...
 *    int get_x() const
 *    { const static SG::ConstAccessor<int> acc ("x", "Myclass");
 *      return acc (*this); }
 *    int& get_x()
 *    { const static SG::Accessor<int> acc ("x", "Myclass");
 *      return acc (*this); }
 @endcode
 *
 * In addition, one sometimes wants to add additional auxiliary data to
 * an existing const container; for example, after a container has been
 * retrieved from StoreGate.  This is called `decoration', and is handled
 * by the @c Decorator object, which is much like @c Accessor and
 * @c ConstAccessor.  The difference
 * is that @c Decorator can take a const container and return a non-const,
 * modifiable reference.  If the container has been locked by calling
 * @c StoreGateSvc::setConst, then this is allowed only if this is a new
 * auxiliary item, in which case it is marked as a decoration, or if it
 * is already marked as a decoration.  This prevents changing existing
 * variables in a locked container.  An @c auxdecor method is also
 * available, analogous to @c auxdata.
 *
 * In addition to the above, the class @c TypelessConstAccessor is a
 * non-templated class that allows access to auxiliary data items
 * directly as a `void *`.  This is useful for code which operates
 * on auxiliary data generically; it shouldn't really be used
 * in other contexts.
 *
 * Normally, an object can have auxiliary data only when it is part
 * of a container.  But sometimes it is useful to be able to associate
 * aux data with an object before it has been added to a container.
 * You can enable this by creating a `private store' for the
 * object with @c makePrivateStore.  This can optionally take
 * an argument from which aux data should be copied.
 * (Using a private store adds overhead, which is why it is not enabled
 * by default.)  Example:
 *
 *@code
 *  class Myclass : public SG::AuxElement { ... };
 *  ...
 *  SG::Accessor<int> myint ("myint");
 *  const Myclass* m = new Myclass;
 *  m->makePrivateStore();
 *  myint(*m) = 10;
 *  DataVector<Myclass> v;
 *  v.push_back(m);
 *  assert (myint(v[0]) == 10);
 @endcode
 *
 * When an object with a private store is added to a container,
 * the aux data is copied to the container and the private store
 * is released.  However, the fact that we had a private store
 * is remembered; if the object is later removed from the container,
 * the private store will be remade, and the aux data will be copied
 * back from the container to the private store.  To explicitly
 * release the private store (so that it won't come back automatically),
 * call @c releasePrivateStore.
 *
 * If you add @c makePrivateStore calls to the constructors of your class,
 * then you should be able to treat aux data as if it were part
 * of the object itself; the required copying will be handled automatically.
 *
 *@code
 *  class Myclass : public SG::AuxElement
 *  {
 *  public:
 *    Myclass() { makePrivateStore(); }
 *    Myclass(const Myclass* other) { makePrivateStore(other); }
 @endcode
 *
 * The @c SG::AuxElementComplete template class may be helpful
 * in setting this up.
 *
 * It is also possible to associate one of these objects with an external
 * aux data store.  This is the `standalone' mode.  To do this, use
 * the @c setStore methods, exactly as you would for a container
 * that has aux data.  @c setStore will throw an exception if the
 * object is a member of a container or has a private store.
 *
 * This class should not have any virtual methods (to avoid forcing
 * derived classes to have a vtable).
 */
class AuxElement
#ifdef ATHCONTAINERS_R21_COMPAT
  : public SG::IAuxElement
#else
  : public ConstAuxElement
#endif
{
public:
  /// Default constructor.
  AuxElement();


  /**
   * @brief Constructor with explicit container / index.
   * @param container Container of which this element will be a part.
   * @param index Index of this element within the container.
   *
   * This does not make any changes to aux data.
   */
  AuxElement (SG::AuxVectorData* container,  size_t index);



  /**
   * @brief Copy Constructor.
   * @param other Object being copied.
   *
   * We do not copy the container/index --- the new object is not yet
   * in a container!
   *
   * In the case of constructing an object with a private store,
   * @c makePrivateStore will take care of copying the aux data.
   */
  AuxElement (const AuxElement& other);


  /**
   * @brief Assignment.
   * @param other The object from which we're assigning.
   *
   * We don't copy container/index, as assignment doesn't change where
   * this object is.  However, if we have aux data, then we copy aux data
   * if we're copying from an object that also has it; otherwise,
   * if we're copying from an object with no aux data, then we clear ours.
   */
  AuxElement& operator= (const AuxElement& other);


  /**
   * @brief Destructor.
   *
   * Any private store is deleted.
   */
  ~AuxElement();


  /**
   * @brief Return the container holding this element.
   */
  const SG::AuxVectorData* container() const;


  /**
   * @brief Return the container holding this element.
   */
  SG::AuxVectorData* container();


  /**
   * @brief Return the index of this element within its container.
   *
   * Inherited from IAuxElement.
   */
  using IAuxElement::index;


  using TypelessConstAccessor = ConstAuxElement::TypelessConstAccessor;

  template <class T, class ALLOC = AuxAllocator_t<T> >
  using ConstAccessor = SG::ConstAccessor<T, ALLOC>;

  template <class T, class ALLOC = AuxAllocator_t<T> >
  using Accessor = SG::Accessor<T, ALLOC>;

  template <class T, class ALLOC = AuxAllocator_t<T> >
  using Decorator = SG::Decorator<T, ALLOC>;


  /**
   * @brief Fetch an aux data variable, as a non-const reference.
   * @param name Name of the aux variable.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor class.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename Accessor<T, ALLOC>::reference_type
  auxdata (const std::string& name);


  /**
   * @brief Fetch an aux data variable, as a non-const reference.
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor class.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename Accessor<T, ALLOC>::reference_type
  auxdata (const std::string& name,
           const std::string& clsname);


  /**
   * @brief Fetch an aux data variable, as a const reference.
   * @param name Name of the aux variable.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor
   * or @c ConstAccessor classes.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename Accessor<T, ALLOC>::const_reference_type
  auxdata (const std::string& name) const;


  /**
   * @brief Fetch an aux data variable, as a const reference.
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor
   * or @c ConstAccessor classes.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename Accessor<T, ALLOC>::const_reference_type
  auxdata (const std::string& name,
           const std::string& clsname) const;


  /**
   * @brief Fetch an aux data variable, as a const reference.
   * @param name Name of the aux variable.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c ConstAccessor class.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename Accessor<T, ALLOC>::const_reference_type
  auxdataConst (const std::string& name) const;


  /**
   * @brief Fetch an aux data variable, as a const reference.
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c ConstAccessor class.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename Accessor<T, ALLOC>::const_reference_type
  auxdataConst (const std::string& name,
                const std::string& clsname) const;


  /**
   * @brief Check if an aux variable is available for reading
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor class.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  bool isAvailable (const std::string& name,
                    const std::string& clsname = "") const;


  /**
   * @brief Check if an aux variable is available for writing
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor class.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  bool isAvailableWritable (const std::string& name,
                            const std::string& clsname = "");


  /**
   * @brief Check if an aux variable is available for writing as a decoration.
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor class.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  bool isAvailableWritableAsDecoration (const std::string& name,
                                        const std::string& clsname = "") const;


  /**
   * @brief Fetch an aux decoration, as a non-const reference.
   * @param name Name of the aux variable.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor class.
   *
   * If the container is locked, this will allow fetching only variables
   * that do not yet exist (in which case they will be marked as decorations)
   * or variables already marked as decorations.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename Decorator<T, ALLOC>::reference_type
  auxdecor (const std::string& name) const;


  /**
   * @brief Fetch an aux decoration, as a non-const reference.
   * @param name Name of the aux variable.
   * @param clsname The name of the associated class.  May be blank.
   *
   * This method has to translate from the aux data name to the internal
   * representation each time it is called.  Using this method
   * inside of loops is discouraged; instead use the @c Accessor class.
   *
   * If the container is locked, this will allow fetching only variables
   * that do not yet exist (in which case they will be marked as decorations)
   * or variables already marked as decorations.
   */
  template <class T, class ALLOC = AuxAllocator_t<T> >
  typename Decorator<T, ALLOC>::reference_type
  auxdecor (const std::string& name,
            const std::string& clsname) const;


 /**
   * @brief Create a new (empty) private store for this object.
   *
   * @c ExcBadPrivateStore will be thrown if this object is already
   * associated with a store.
   */
  void makePrivateStore();


  /**
   * @brief Create a new private store for this object and copy aux data.
   * @param other The object from which aux data should be copied.
   *
   * @c ExcBadPrivateStore will be thrown if this object is already
   * associated with a store.
   *
   * If @c other is an object that has aux data, then those data will
   * be copied; otherwise, nothing will be done.
   */
  template <class U1>
  void makePrivateStore (const U1& other);


  /**
   * @brief Create a new private store for this object and copy aux data.
   * @param other The object from which aux data should be copied.
   *
   * @c ExcBadPrivateStore will be thrown if this object is already
   * associated with a store.
   *
   * If @c other is an object that has aux data, then those data will
   * be copied; otherwise, nothing will be done.
   */
  template <class U1>
  void makePrivateStore (const U1* other);


  /**
   * @brief Release and free any private store associated with this object.
   *
   * @c ExcBadPrivateStore will be thrown if this object does not
   * have a private store.
   */
  void releasePrivateStore();


  /**
   * @brief Set the store associated with this object.
   * @param store The new store.
   *
   * If store is nonzero, this adds a standalone store to the object.
   * The object must not be in a container and must not have a private store.
   * If store is zero, this removes a standalone store.
   */
  void setStore (const SG::IConstAuxStore* store);


  /**
   * @brief Set the store associated with this object.
   * @param store The new store.
   *
   * If store is nonzero, this adds a standalone store to the object.
   * The object must not be in a container and must not have a private store.
   * If store is zero, this removes a standalone store.
   */
  void setStore (SG::IAuxStore* store);


  /**
   * @brief Set the store associated with this object.
   * @param store The new store.
   *
   * If store is nonzero, this adds a standalone store to the object.
   * The object must not be in a container and must not have a private store.
   * If store is zero, this removes a standalone store.
   */
  void setStore (const DataLink< SG::IConstAuxStore >& store);


  /**
   * @brief Synonym for @c setStore with @c IConstAuxStore.
   * @param store The new store.
   */
  void setConstStore (const SG::IConstAuxStore* store);


  /**
   * @brief Synonym for @c setStore with @c IAuxStore.
   * @param store The new store.
   */
  void setNonConstStore (SG::IAuxStore* store);


  /**
   * @brief Test to see if this object is currently using a private store.
   */
  bool usingPrivateStore() const;


  /**
   * @brief Test to see if this object is currently using a standalone store.
   */
  bool usingStandaloneStore() const;


  /**
   * @brief Return the current store, as a const interface.
   *
   * This will be non-zero if either a const or non-const store
   * is associated with this object.
   * This will fetch either a private or standalone store.
   */
  const SG::IConstAuxStore* getConstStore() const;


  /**
   * @brief Return the current store, as a non-const interface.
   *
   * This will be non-zero if a non-const store is associated with this object.
   * This will fetch either a private or standalone store.
   */
  SG::IAuxStore* getStore() const;


  /**
   * @brief Clear the cached aux data pointers.
   *
   * You should call this any time something changes in the aux store
   * that could invalidate the vector pointers.
   */
  void clearCache();


  /**
   * @brief Return a set of identifiers for existing data items
   *        for this object.
   *
   *        If this object has a private or standalone store, then information
   *        from that will be returned.  Otherwise, if this element
   *        is part of a container, then information for the container
   *        will be returned.  Otherwise, return an empty set.
   */
  const SG::auxid_set_t& getAuxIDs() const;


  /**
   * @brief Return true if this object has an associated store.
   *
   * This will be true for either a private or standalone store.
   */
  bool hasStore() const;


  /**
   * @brief Return true if this object has an associated non-const store.
   *
   * This will be true for either a private or standalone store.
   */
  bool hasNonConstStore() const;


  /**
   * @brief Clear all decorations.
   *
   * Erase all decorations from an associated store, restoring the state to when
   * @c lock was called.
   *
   * Returns true if there were any decorations that were cleared,
   * false if the store did not contain any decorations.
   */
  bool clearDecorations() const;


  /**
   * @brief Return true if index tracking is enabled for this object.
   *
   * Always returns true.  Included here to be consistent with AuxVectorBase
   * when standalone objects may be used as template parameters.
   */
  bool trackIndices() const;


  /// Mark that this type supports thinning operations.
  /// See AthContainers/supportsThinning.h and
  /// AthenaPoolCnvSvc/T_AthenaPoolCnv.h.
  /// Helps guide which pool converter template will be used.
  /// If false, the default pool converter will be used
  /// rather than the aux store-specific one.
  /// Ordinary xAOD type should not touch this, but
  /// may be overridden in a derived class to handle
  /// certain special cases.
  static constexpr bool supportsThinning = true;


private:
  friend class SG::ConstAuxElement;
  friend class SG::AuxVectorBase;
  friend class SG::AuxVectorBase_test;


  /**
   * @brief Out-of-line portion of destructor.
   *
   * Delete a private store if we have one.
   */
  void releasePrivateStoreForDtor();


  /**
   * @brief Set the index/container for this element.
   * @param index The index of this object within the container.
   * @param container The container holding this object.
   *                  May be null if this object is being removed
   *                  from a container.
   *
   * Usually this simply sets the index and container members
   * of this object.  However, in the case where this object has
   * an associated private store, then we need to deal with
   * releasing the store if the object is being added to a container,
   * or making a new store if the object is being removed from a container.
   */
  void setIndex (size_t index, SG::AuxVectorData* container);


  /**
   * @brief Set the index/container for this element.
   * @param index The index of this object within the container.
   * @param container The container holding this object.
   *                  May be null if this object is being removed
   *                  from a container.
   *
   * This is called from @c setIndex when we have a private store to deal with.
   */
  bool setIndexPrivate (size_t index, SG::AuxVectorData* container);


  /**
   * @brief Create a new private store for this object and copy aux data.
   * @param other The object from which aux data should be copied.
   *
   * @c ExcBadPrivateStore will be thrown if this object is already
   * associated with a store.
   *
   * This overload handles the case where @c other does not have aux data.
   */
  void makePrivateStore1 (const void*);


  /**
   * @brief Create a new private store for this object and copy aux data.
   * @param other The object from which aux data should be copied.
   *
   * @c ExcBadPrivateStore will be thrown if this object is already
   * associated with a store.
   *
   * This overload handles the case where @c other does have aux data.
   */
  void makePrivateStore1 (const AuxElement* other);


  /**
   * @brief Set the store associated with this object.
   * @param store The new store.
   *
   * Helper for @c setStore.  Creates the @c AuxElementStandaloneData
   * object if needed and returns it.
   */
  AuxElementStandaloneData* setStore1 (const SG::IConstAuxStore* store);


  /**
   * @brief Clear all aux data associated with this element.
   *
   * If this object has no associated store, this does nothing.
   * If the associated aux data is const, this throws @c ExcConstAuxData.
   */
  void clearAux();


  /**
   * @brief Copy aux data from another object.
   * @param other The object from which to copy.
   *
   * If this object has no associated store, this does nothing.
   * If the associated aux data is const, this throws @c ExcConstAuxData.
   *
   * All aux data items from @c other are copied to this object.
   * Any aux data items associated with this object that are not present
   * in @c other are cleared.  (If @c other has no aux data, then all
   * aux data items for this object are cleared.)
   */
  void copyAux (const ConstAuxElement& other);


#ifdef ATHCONTAINERS_R21_COMPAT
  /**
   * @brief Copy aux data from another object.
   * @param other The object from which to copy.
   *
   * If this object has no associated store, this does nothing.
   * If the associated aux data is const, this throws @c ExcConstAuxData.
   *
   * All aux data items from @c other are copied to this object.
   * Any aux data items associated with this object that are not present
   * in @c other are cleared.  (If @c other has no aux data, then all
   * aux data items for this object are cleared.)
   */
  void copyAux (const AuxElement& other);


  /// The container of which this object is an element.
  /// Should be null if this object is not within a container,
  /// except that it may also point at a private store.
  SG::AuxVectorData* m_container;
#endif
};


} // namespace SG


#include "AthContainers/AuxElement.icc"


#ifndef XAOD_STANDALONE
CLASS_DEF (SG::AuxElement, 225182422, 1)
#endif // not XAOD_STANDALONE


#endif // not ATHCONTAINERS_AUXELEMENTBASE_H
