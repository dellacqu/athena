
/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONTESTER_MUONTESTERBRANCH_IXX
#define MUONTESTER_MUONTESTERBRANCH_IXX
#include <TTree.h>
#include <algorithm> //for std::find
namespace MuonVal {
template <class T> bool MuonTesterBranch::addToTree(T& variable) {
    if (initialized()) {
        ATH_MSG_INFO("init()  -- " << name() << " is already initialized.");
        return true;
    }
    std::string bName = eraseWhiteSpaces(name());
    if (bName.empty() || !m_tree) {
        ATH_MSG_ERROR("init()  -- Empty names are forbidden.");
        return false;
    } else if (m_tree->FindBranch(bName.c_str())) {
        ATH_MSG_ERROR("The branch " << name() << " already exists in TTree " << m_tree->GetName() << ".");
        return false;
    } else if (!m_tree->Branch(bName.c_str(), &variable)) {
        ATH_MSG_ERROR("Could not create the branch " << name() << " in TTree " << m_tree->GetName() << ".");
        return false;
    }
    m_init = true;
    return true;
}
template <class Key>
bool MuonTesterBranch::declare_dependency(Key& key) {
    if (std::find(m_dependencies.begin(), m_dependencies.end(),&key) != m_dependencies.end()) return true;
    if (!key.initialize(!key.empty())) return false;
    m_dependencies.emplace_back(&key);
    ATH_MSG_DEBUG("Declared new dependency "<<key.fullKey()<<" to "<<name());
    return true;
}
}
#endif
