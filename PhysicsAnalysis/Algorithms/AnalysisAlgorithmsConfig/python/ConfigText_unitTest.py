#!/usr/bin/env python
#
# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# @author Joseph Lambert

def compareConfigSeq(seq1, seq2, *, checkOrder=False):
    """Compares two ConfigSequences"""
    blocks1 = seq1._blocks
    blocks2 = seq2._blocks
    print("Block order for each config sequence")
    print("\033[4m{0:<30} {1:<30}\033[0m".format("Sequence1", "Sequence2"))
    for i in range(max(len(blocks1), len(blocks2))):
        name1, name2 = '', ''
        if i < len(blocks1):
            name1 = blocks1[i].__class__.__name__
        if i < len(blocks2):
            name2 = blocks2[i].__class__.__name__
        print(f"{name1:<30} {name2}")
    if not checkOrder:
        print("Sorting blocks by name (will not sort blocks with same name)")
        blocks1.sort(key=lambda x: x.__class__.__name__)
        blocks2.sort(key=lambda x: x.__class__.__name__)
    if len(blocks1) != len(blocks2):
        raise Exception("Number of blocks are different")
    for i in range(len(blocks1)):
        block1 = blocks1[i]
        block2 = blocks2[i]
        name1 = block1.__class__.__name__
        name2 = block2.__class__.__name__
        if name1 != name2:
            raise Exception(f"In position {i} "
                f"the first sequence results in {name1} "
                f"and the second sequence results in {name2}")
        for name in block1.getOptions():
            if name == 'groupName':
                continue
            value1 = block1.getOptionValue(name)
            value2 = block2.getOptionValue(name)
            if value1 != value2:
                raise Exception(f"For block {name1}, the block "
                    f"option {name} the first sequence results in {value1} "
                    f"and the second sequence results in {value2}")


def compareTextBuilder(yamlPath='', *, checkOrder=False) :
    """
    Create a configSequence using provided YAML file and a
    configSequence using ConfigText python commands and compare.

    Will raise an exception if configSequences differ
    """
    # create text config object to build text configurations
    from AnalysisAlgorithmsConfig.ConfigText import TextConfig
    config = TextConfig()

    # CommonServices
    config.addBlock('CommonServices')
    config.setOptions(systematicsHistogram='systematicsList')
    config.setOptions(filterSystematics="^(?:(?!PseudoData).)*$")

    # PileupReweighting
    config.addBlock('PileupReweighting')

    # EventCleaning
    config.addBlock('EventCleaning')
    config.setOptions (runEventCleaning=True)

    # Jets
    config.addBlock('Jets')
    config.setOptions (containerName='AnaJets')
    config.setOptions (jetCollection='AntiKt4EMPFlowJets')
    config.setOptions (runJvtUpdate=False)
    config.setOptions (runNNJvtUpdate=True)
    # Jets.FlavourTagging
    config.addBlock( 'Jets.FlavourTagging')
    config.setOptions (containerName='AnaJets')
    config.setOptions (selectionName='ftag')
    config.setOptions (noEffSF=True)
    config.setOptions (btagger='DL1dv01')
    config.setOptions (btagWP='FixedCutBEff_60')
    config.setOptions (saveScores='All')
    # Jets.JVT
    config.addBlock('Jets.JVT', containerName='AnaJets')
    # Jets.PtEtaSelection
    config.addBlock ('Jets.PtEtaSelection')
    config.setOptions (containerName='AnaJets')
    config.setOptions (selectionDecoration='selectPtEta')

    # Electrons
    config.addBlock ('Electrons')
    config.setOptions (containerName='AnaElectrons')
    config.setOptions (forceFullSimConfig=True)
    # Electrons.WorkingPoint
    config.addBlock ('Electrons.WorkingPoint')
    config.setOptions (containerName='AnaElectrons')
    config.setOptions (selectionName='loose')
    config.setOptions (forceFullSimConfig=True)
    config.setOptions (noEffSF=True)
    config.setOptions (likelihoodWP='LooseBLayerLH')
    config.setOptions (isolationWP='Loose_VarRad')
    config.setOptions (recomputeLikelihood=False)
    config.setOptions (writeTrackD0Z0=True)
    # Electrons.PtEtaSelection
    config.addBlock ('Electrons.PtEtaSelection')
    config.setOptions (containerName='AnaElectrons')
    config.setOptions (selectionDecoration='selectPtEta')
    config.setOptions (minPt=10000.0)

    # Photons
    config.addBlock ('Photons', containerName='AnaPhotons')
    config.setOptions (forceFullSimConfig=True)
    config.setOptions (recomputeIsEM=False)
    # Photons.WorkingPoint
    config.addBlock ('Photons.WorkingPoint')
    config.setOptions (containerName='AnaPhotons')
    config.setOptions (selectionName='tight')
    config.setOptions (forceFullSimConfig=True)
    config.setOptions (noEffSF=True)
    config.setOptions (qualityWP='Tight')
    config.setOptions (isolationWP='FixedCutTight')
    config.setOptions (recomputeIsEM=False)
    # Photons.PtEtaSelection
    config.addBlock ('Photons.PtEtaSelection')
    config.setOptions (containerName='AnaPhotons')
    config.setOptions (selectionDecoration='selectPtEta')
    config.setOptions (minPt=10000.0)

    # Muons
    config.addBlock ('Muons', containerName='AnaMuons')
    # Muons.WorkingPoint
    config.addBlock ('Muons.WorkingPoint')
    config.setOptions (containerName='AnaMuons')
    config.setOptions (selectionName='medium')
    config.setOptions (quality='Medium')
    config.setOptions (isolation='Loose_VarRad')
    config.setOptions (onlyRecoEffSF=True)
    config.setOptions (writeTrackD0Z0=True)
    # Muons.PtEtaSelection
    config.addBlock ('Muons.PtEtaSelection')
    config.setOptions (containerName='AnaMuons')
    config.setOptions (selectionDecoration='selectPtEta')

    # TauJets
    config.addBlock ('TauJets', containerName='AnaTauJets')
    # TauJets.WorkingPoint
    config.addBlock ('TauJets.WorkingPoint')
    config.setOptions (containerName='AnaTauJets')
    config.setOptions (selectionName='tight')
    config.setOptions (quality='Tight')
    # TauJets.PtEtaSelection
    config.addBlock ('TauJets.PtEtaSelection')
    config.setOptions (containerName='AnaTauJets')
    config.setOptions (selectionDecoration='selectPtEta')

    config.addBlock ('SystObjectLink')
    config.setOptions (containerName='AnaJets')
    config.addBlock ('SystObjectLink')
    config.setOptions (containerName='AnaElectrons')
    config.addBlock ('SystObjectLink')
    config.setOptions (containerName='AnaPhotons')
    config.addBlock ('SystObjectLink')
    config.setOptions (containerName='AnaMuons')
    config.addBlock ('SystObjectLink')
    config.setOptions (containerName='AnaTauJets')

    config.addBlock ('ObjectCutFlow')
    config.setOptions (containerName='AnaJets')
    config.setOptions (selectionName='jvt')
    config.addBlock ('ObjectCutFlow')
    config.setOptions (containerName='AnaElectrons')
    config.setOptions (selectionName='loose')
    config.addBlock ('ObjectCutFlow')
    config.setOptions (containerName='AnaPhotons')
    config.setOptions (selectionName='tight')
    config.addBlock ('ObjectCutFlow')
    config.setOptions (containerName='AnaMuons')
    config.setOptions (selectionName='medium')
    config.addBlock ('ObjectCutFlow')
    config.setOptions (containerName='AnaTauJets')
    config.setOptions (selectionName='tight')

    # GeneratorLevelAnalysis
    config.addBlock( 'GeneratorLevelAnalysis')
    config.setOptions (saveCutBookkeepers=True)
    config.setOptions (runNumber=284500)
    config.setOptions (cutBookkeepersSystematics=True)

    # MissingET
    config.addBlock ('MissingET')
    config.setOptions (containerName='AnaMET')
    config.setOptions (jets='AnaJets')
    config.setOptions (taus='AnaTauJets.tight')
    config.setOptions (electrons='AnaElectrons.loose')
    config.setOptions (photons='AnaPhotons.tight')
    config.setOptions (muons='AnaMuons.medium')

    # OverlapRemoval
    config.addBlock( 'OverlapRemoval' )
    config.setOptions (inputLabel='preselectOR')
    config.setOptions (outputLabel='passesOR' )
    config.setOptions (jets='AnaJets')
    config.setOptions (taus='AnaTauJets.tight')
    config.setOptions (electrons='AnaElectrons.loose')
    config.setOptions (photons='AnaPhotons.tight')
    config.setOptions (muons='AnaMuons.medium')

    # Thinning
    config.addBlock ('Thinning')
    config.setOptions (containerName='AnaElectrons')
    config.setOptions (selectionName='loose')
    config.setOptions (outputName='OutElectrons')
    config.addBlock ('Thinning')
    config.setOptions (containerName='AnaPhotons')
    config.setOptions (selectionName='tight')
    config.setOptions (outputName='OutPhotons')
    config.addBlock ('Thinning')
    config.setOptions (containerName='AnaMuons')
    config.setOptions (selectionName='medium')
    config.setOptions (outputName='OutMuons')
    config.addBlock ('Thinning')
    config.setOptions (containerName='AnaTauJets')
    config.setOptions (selectionName='tight')
    config.setOptions (outputName='OutTauJets')
    config.addBlock ('Thinning')
    config.setOptions (containerName='AnaJets')
    config.setOptions (outputName='OutJets')

    config.addBlock ('Output')
    config.setOptions (treeName='analysis')
    config.setOptions (vars=[])
    config.setOptions (metVars=[])
    outputContainers = {
        'mu_': 'OutMuons',
        'el_': 'OutElectrons',
        'ph_' : 'OutPhotons',
        'tau_': 'OutTauJets',
        'jet_': 'OutJets',
        'met_': 'AnaMET',
        '': 'EventInfo'}
    config.setOptions (containers=outputContainers)
    disable_commands = [
        'disable jet_select_baselineJvt.*',
        'disable mu_select_medium.*',
        'disable ph_select_tight.*',
        'disable tau_select_tight.*',
        'disable el_select_loose.*',
    ]
    config.setOptions (commands=disable_commands)

    # configure ConfigSequence
    configSeq = config.configure()

    # create text config object to build text configurations
    textConfig = TextConfig(yamlPath)
    textConfigSeq = textConfig.configure()

    # compare - will raise error if False
    compareConfigSeq(configSeq, textConfigSeq, checkOrder=checkOrder)


def compareBlockConfig(yamlPath='', *, checkOrder=False) :
    """
    Create a configSequence using provided YAML file and a
    configSequence using the block configuration and compare.

    Will raise an exception if configSequences differ
    """
    # create configSeq for block configuration
    from AnalysisAlgorithmsConfig.FullCPAlgorithmsTest import makeSequenceBlocks
    configSeq = makeSequenceBlocks(dataType='fullsim', algSeq=None,
            forCompare=True, isPhyslite=False, forceEGammaFullSimConfig=True,
            returnConfigSeq=True)

    # create text config object to build text configurations
    from AnalysisAlgorithmsConfig.ConfigText import TextConfig
    textConfig = TextConfig(yamlPath)
    textConfigSeq = textConfig.configure()

    # compare - will raise error if False
    compareConfigSeq(configSeq, textConfigSeq, checkOrder=checkOrder)


if __name__ == '__main__':
    import os
    import optparse
    parser = optparse.OptionParser()
    parser.add_option('--text-config', dest='text_config',
            default='', action='store',
            help='YAML file used in unit test')
    parser.add_option('--compare-block', dest='compare_block',
            default=False, action='store_true',
            help='Compare config sequence from YAML and block configuration')
    parser.add_option('--compare-builder', dest='compare_builder',
            default=False, action='store_true',
            help='Compare config sequence from YAML and python configuration')
    parser.add_option('--check-order', dest='check_order',
            default=False, action='store_true',
            help='Require blocks to be in the same order')
    (options, args) = parser.parse_args()
    textConfig = options.text_config
    compareBlock = options.compare_block
    compareBuilder = options.compare_builder
    checkOrder = options.check_order

    if not os.path.isfile(textConfig):
        raise FileNotFoundError(f"{textConfig} is not a file")

    # compare YAML and builder
    if compareBuilder:
        print("Comparing config sequences from the block and text"
            "configuration methods")
        compareTextBuilder(textConfig, checkOrder=checkOrder)
    # compare YAML and block config
    if compareBlock:
        print("Comparing config sequences from the block and block"
            "configuration methods")
        compareBlockConfig(textConfig, checkOrder=checkOrder)
