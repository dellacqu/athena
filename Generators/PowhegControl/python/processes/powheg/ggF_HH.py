# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
import os
from AthenaCommon import Logging
from ..powheg_V2 import PowhegV2

## Get handle to Athena logging
logger = Logging.logging.getLogger("PowhegControl")


class ggF_HH(PowhegV2):
    """! Default Powheg configuration for gluon-fusion Higgs boson production with quark mass and EW effects.

    Create a configurable object with all applicable Powheg options.

    @author James Robinson  <james.robinson@cern.ch>
    """

    def __init__(self, base_directory, **kwargs):
        """! Constructor: all process options are set here.

        @param base_directory: path to PowhegBox code.
        @param kwargs          dictionary of arguments from Generate_tf.
        """
        super(ggF_HH, self).__init__(base_directory, "ggHH", **kwargs)

        # Add grid file creation function
        self.validation_functions.append("create_grid_file")

        # This process uses a python script which uses .grid files searched for in $PYTHONPATH
        # By appending the folder that they live in to PYTHONPATH it is able to find them
        # At the moment these files are stored or linked locally, so we use ${PWD}
        # but we may use self.executable.replace("pwhg_main", "Virtual") instead at some point
        os.environ["PYTHONPATH"] += ":" + os.environ["PWD"]

        # need to use libraries compatible with the environment used at compilation (so centos7)
        # would need to use ${LHAPDF_INSTAL_PATH}/lib/python3.9/site-packages when/if the process compiles in alma9 eventually
        lhapdf_python_path = "/cvmfs/sft.cern.ch/lcg/releases/LCG_101/MCGenerators/lhapdf/6.3.0/x86_64-centos7-gcc8-opt/lib/python3.9/site-packages"
        os.environ["PYTHONPATH"] += ":" + lhapdf_python_path

        logger.info('PYTHONPATH is now:\n{}'.format(os.environ["PYTHONPATH"]))

        # Add all keywords for this process, overriding defaults if required
        self.add_keyword("alphas_from_lhapdf")
        self.add_keyword("bornktmin")
        self.add_keyword("bornonly")
        self.add_keyword("bornsuppfact")
        self.add_keyword("bornzerodamp")
        self.add_keyword("bottomthr")
        self.add_keyword("bottomthrpdf")
        self.add_keyword("btildeborn")
        self.add_keyword("btildecoll")
        self.add_keyword("btildereal")
        self.add_keyword("btildevirt")
        self.add_keyword("btlscalect")
        self.add_keyword("btlscalereal")
        self.add_keyword("charmthr")
        self.add_keyword("charmthrpdf")
        self.add_keyword("check_bad_st2")
        self.add_keyword("chhh", 1.0)
        self.add_keyword("ct", 1.0)
        self.add_keyword("ctt", 0.)
        self.add_keyword("cggh", 0.)
        self.add_keyword("cgghh", 0.)
        self.add_keyword("clobberlhe")
        self.add_keyword("colltest")
        self.add_keyword("compress_lhe")
        self.add_keyword("compress_upb")
        self.add_keyword("compute_rwgt")
        self.add_keyword("doublefsr")
        self.add_keyword("evenmaxrat")
        self.add_keyword("facscfact", self.default_scales[0])
        self.add_keyword("fastbtlbound")
        self.add_keyword("fixedgrid")
        self.add_keyword("fixedscale", description="Set renormalisation and factorisation scales to 2*m_H")
        self.add_keyword("flg_debug")
        self.add_keyword("foldcsi", 2)
        self.add_keyword("foldphi", 2)
        self.add_keyword("foldy", 5)
        self.add_keyword("fullrwgt")
        self.add_keyword("gfermi")
        self.add_keyword("hdamp")
        self.add_keyword("hdecaymode")
        self.add_keyword("hfact")
        self.add_keyword("hmass", 125.)
        self.add_keyword("icsimax", 2)
        self.add_keyword("ih1")
        self.add_keyword("ih2")
        self.add_keyword("itmx1", 2)
        self.add_keyword("itmx1rm")
        self.add_keyword("itmx2", 4)
        self.add_keyword("itmx2rm")
        self.add_keyword("iupperfsr")
        self.add_keyword("iupperisr")
        self.add_keyword("iymax", 2)
        self.add_keyword("lhans1", self.default_PDFs)
        self.add_keyword("lhans2", self.default_PDFs)
        self.add_keyword("lhapdf6maxsets")
        self.add_keyword("lhrwgt_descr")
        self.add_keyword("lhrwgt_group_combine")
        self.add_keyword("lhrwgt_group_name")
        self.add_keyword("lhrwgt_id")
        self.add_keyword("LOevents")
        self.add_keyword("manyseeds")
        self.add_keyword("max_io_bufsize")
        self.add_keyword("maxseeds")
        self.add_keyword("minlo", frozen=True)
        self.add_keyword("mintupbratlim")
        self.add_keyword("mintupbxless")
        self.add_keyword("mtdep", 3)
        self.add_keyword("ncall1", 10000)
        self.add_keyword("ncall1rm")
        self.add_keyword("ncall2", 15000)
        self.add_keyword("ncall2rm")
        self.add_keyword("ncallfrominput")
        self.add_keyword("noevents")
        self.add_keyword("novirtual")
        self.add_keyword("nubound", 20000)
        self.add_keyword("olddij")
        self.add_keyword("par_2gsupp")
        self.add_keyword("par_diexp")
        self.add_keyword("par_dijexp")
        self.add_keyword("parallelstage")
        self.add_keyword("pdfreweight")
        self.add_keyword("ptHHcut_CT")
        self.add_keyword("ptHHcut")
        self.add_keyword("ptsqmin")
        self.add_keyword("ptsupp")
        self.add_keyword("radregion")
        self.add_keyword("rand1")
        self.add_keyword("rand2")
        self.add_keyword("renscfact", self.default_scales[1])
        self.add_keyword("rescue_reals")
        self.add_keyword("rwl_add")
        self.add_keyword("rwl_file")
        self.add_keyword("rwl_format_rwgt")
        self.add_keyword("rwl_group_events")
        self.add_keyword("skipextratests")
        self.add_keyword("smartsig")
        self.add_keyword("softtest")
        self.add_keyword("stage2init")
        self.add_keyword("storeinfo_rwgt")
        self.add_keyword("storemintupb")
        self.add_keyword("testplots")
        self.add_keyword("testsuda")
        self.add_keyword("topmass", 173.) # this value is hardcoded in the virtual matrix element, and for consistency has not to be changed when running full theory prediction (i.e. mtdep=3)
        self.add_keyword("ubexcess_correct")
        self.add_keyword("ubsigmadetails")
        self.add_keyword("use-old-grid")
        self.add_keyword("use-old-ubound")
        self.add_keyword("withdamp")
        self.add_keyword("withnegweights")
        self.add_keyword("withsubtr")
        self.add_keyword("Wmass")
        self.add_keyword("Wwidth")
        self.add_keyword("xgriditeration")
        self.add_keyword("xupbound")
        self.add_keyword("zerowidth")
        self.add_keyword("Zmass")
        self.add_keyword("Zwidth")

    def create_grid_file(self):
        """! Creates the .grid file needed by this process."""
        """! This function calls a python script provided by the authors, which is linked to the local directory."""
        """! The code of this function is adapted from the script ${POWHEGPATH}/POWHEG-BOX-V2/ggHH/testrun/run.sh"""
        self.expose()  # convenience call to simplify syntax

        logger.info('Now attempting to link locally the files needed by this Powheg process')
        try:
            os.system("ln -s " + os.environ["POWHEGPATH"] + "/POWHEG-BOX-V2/ggHH/Virtual/events.cdf events.cdf")
            os.system("ln -s " + os.environ["POWHEGPATH"] + "/POWHEG-BOX-V2/ggHH/Virtual/creategrid.py creategrid.py")
            os.system("for grid in " + os.environ["POWHEGPATH"] + "/POWHEG-BOX-V2/ggHH/Virtual/Virt_full_*E*.grid; do ln -s $grid ${grid##*/}; done")
        except RuntimeError:
            logger.error('Impossible to link the needed files locally')
            raise

        # need to override lhapdf python path while the powheg process has been compiled in a different platform
        py_path_save = os.environ["PYTHONPATH"]
        py_path_temp = os.environ["LHAPDF_INSTAL_PATH"] + "/lib/python3.9/site-packages" + ":" + py_path_save
        os.environ["PYTHONPATH"] = py_path_temp
        logger.debug(f'Temporarily setting PYTHONPATH to:\n{py_path_temp}')

        # handling the parameters of this process
        # these parameters need to be parsed in a specific format
        chhh_str = f'{list(self.parameters_by_keyword("chhh"))[0].value:+.4E}'
        ct_str   = f'{list(self.parameters_by_keyword("ct"))[0].value:+.4E}'
        ctt_str  = f'{list(self.parameters_by_keyword("ctt"))[0].value:+.4E}'
        cggh_str   = f'{list(self.parameters_by_keyword("cggh"))[0].value:+.4E}'
        cgghh_str  = f'{list(self.parameters_by_keyword("cgghh"))[0].value:+.4E}'
        grid_file_name = f'Virt_full_{chhh_str}_{ct_str}_{ctt_str}_{cggh_str}_{cgghh_str}.grid'

        logger.info('Now trying to use creategrid.py to create the Virt_full_*.grid file')
        logger.info(f'File name: {grid_file_name}')
        logger.info(f'Parameters are: chhh={chhh_str}, ct={ct_str}, ctt={ctt_str}, cggh={cggh_str}, cgghh={cgghh_str}')
        try:
            #import creategrid as cg
            #cg.combinegrids(grid_file_name, chhh_str, ct_str, ctt_str, cggh_str, cgghh_str)
            pythoncmd=f"import creategrid as cg; cg.combinegrids('{grid_file_name}', {chhh_str}, {ct_str}, {ctt_str}, {cggh_str}, {cgghh_str})"
            os.system("python3 -c \""+pythoncmd+"\"")
        except RuntimeError:
            logger.error('Impossible to use creategrid.py to create the Virt_full_*.grid file')
            raise

        # setting PYTHONPATH back to its original value
        os.environ["PYTHONPATH"] = py_path_save
        logger.debug(f'Setting PYTHONPATH back to:\n{py_path_save}')

        logger.info('Although the produced Virt_full_*.grid file now exists in the local directory, Powheg will later try to find it in all directories contained in $PYTHONPATH. This will produce several "not found" info messages which can safely be ignored.')
